unit uMain;

interface

uses
   System.SysUtils, System.Types, System.UITypes, System.Classes,
  System.Variants,
  FMX.Types, FMX.Controls, FMX.Forms, FMX.Graphics, FMX.Dialogs,
  FMX.Controls.Presentation, FMX.StdCtrls, Android.BroadcastReceiver, FMX.Menus, FMX.Edit

   {$IFDEF  ANDROID}
   ,
   FMX.ScrollBox, FMX.Memo,  Androidapi.JNI.GraphicsContentViewText,
   Androidapi.JNI.JavaTypes, Androidapi.JNI.App, Androidapi.Helpers, FMX.EditBox, FMX.SpinBox
   {$ENDIF}
  ;
type
  TfrmMain = class(TForm)
    lbl1: TLabel;
    btnStart: TButton;
    Label1: TLabel;
    Memo1: TMemo;
    lblScanSource: TLabel;
    lblScanData: TLabel;
    edtScan: TLabel;
    lblScanDecoder: TLabel;
    SpinBox1: TSpinBox;
    lblInfo: TLabel;
    procedure btnStartClick(Sender: TObject);
    procedure FormShow(Sender: TObject);
  private
    procedure CreateBroadcastIntent;
    {$IFDEF  ANDROID}
    procedure BroadcastIntent1BroadcastReceived(Context: JContext; Intent: JIntent);
   {$ENDIF}
    { Private declarations }
  public
    { Public declarations }
    BroadcastIntent1: TBroadcastIntent;
  end;

var
  frmMain:     TfrmMain;
  filter:      JIntentFilter;
  battery:     JIntent;
  deviceCount: Integer;

implementation

{$R *.fmx}

 {$IFDEF  ANDROID}

procedure TfrmMain.CreateBroadcastIntent;
begin
  BroadcastIntent1 := TBroadcastIntent.Create(Self);

  BroadcastIntent1.Name := 'BroadcastIntent1';
  BroadcastIntent1.OnBroadcastReceived := BroadcastIntent1BroadcastReceived;
  with BroadcastIntent1.Devices.Add do begin
    Device := 'zebra';
    Details.IntentString := 'com.symbol.datawedge.api.ACTION';
    Details.ExtraString  := 'com.symbol.datawedge.api.SOFT_SCAN_TRIGGER';
    Details.SourceString := 'com.symbol.datawedge.source';
    Details.DataString := 'com.symbol.datawedge.data_string';
    Details.LabelType := 'com.symbol.datawedge.label_type';
    Details.StartScanning := 'START_SCANNING';
    Details.EnableString := 'com.symbol.datawedge.api.ENABLE_DATAWEDGE';
    Details.Category := 'CATEGORY_DEFAULT';
  end;
  with BroadcastIntent1.Devices.Add do begin
    Device := 'datalogic';
    Details.IntentString := 'com.datalogic.decodewedge.decode_action';
    Details.ExtraString  := 'com.datalogic.decode.intentwedge.barcode_string';
    Details.DataString := 'com.datalogic.decode.intentwedge.barcode_string';
    Details.LabelType := 'com.datalogic.decode.intentwedge.barcode_type';
    Details.StartScanning := 'com.datalogic.decodewedge.ACTION_START_DECODE';
    Details.EnableString := 'com.datalogic.decodewedge.ACTION_STOP_DECODE';
    Details.Category := 'com.datalogic.decodewedge.decode_category';
    // Details.Category := 'CATEGORY_DEFAULT';
  end;
  BroadcastIntent1.UseIntentCategory := False;
end;
{$ENDIF}


{$IFDEF  ANDROID}
procedure TfrmMain.BroadcastIntent1BroadcastReceived(Context: JContext; Intent: JIntent);
var
  code:    string;
  s: string ;
  i: Integer;
  fBateryHealth: string ;
  const
  BateryHealthStr: array [1..7] of string =
    ('unknown', 'Good', 'Overhead', 'Dead', 'Over voltage', 'unspecified failure', 'Cold');
begin
  if filter=nil then
  begin
  filter := TJIntentFilter.Create;
  filter.addAction(TJIntent.JavaClass.ACTION_BATTERY_CHANGED);
  battery := Context.registerReceiver(NIL, filter);

  if deviceCount=0 then
     begin
        BroadcastIntent1.AddZebraSettings ;
        filter.addAction(StringToJString('com.symbol.datawedge.api.ACTION'));
        Context.registerReceiver(NIL, filter)       ;
     end;

  if deviceCount=1 then
     begin
        BroadcastIntent1.AddDataLogicSettings ;
        filter.addAction(StringToJString('com.datalogic.decodewedge.decode_action'));
        filter.addCategory(StringToJString('com.datalogic.decodewedge.decode_category'));
        Context.registerReceiver(NIL, filter)       ;
     end;
  end;



  i := battery.getIntExtra(StringToJString('health'), -1) ;
  fBateryHealth := BateryHealthStr[i] ;
  s:='Battery Status: '+fBateryHealth ;

  lbl1.Text := s ;

  i := battery.getIntExtra(StringToJString('health_percentage'), -1) ;

  if deviceCount=0 then
     BroadcastIntent1.displayScanResult(Intent, 'via Broadcast');

  if deviceCount=1 then
     BroadcastIntent1.displayDatalogicScanResult(Intent, 'via Broadcast');



  code := BroadcastIntent1.ScanData;
  if code = '' then
    Exit;
  if not Assigned(Screen.ActiveForm) then
    Exit;
  lbl1.Text := code ;

end;

procedure TfrmMain.btnStartClick(Sender: TObject);
begin
    {$IFDEF  ANDROID}
         deviceCount:= Trunc(SpinBox1.Value);
         CreateBroadcastIntent;
         BroadcastIntent1.AddAction(TJIntent.JavaClass.ACTION_BATTERY_CHANGED);
         BroadcastIntent1.UseIntentCategory := False;
         BroadcastIntent1.DeviceIndex       := deviceCount ;
         BroadcastIntent1.AddZebraSettings ;
         Label1.Text                        :=   BroadcastIntent1.Devices.Category[BroadcastIntent1.DeviceIndex] ;



    if deviceCount=0 then
    begin
     BroadcastIntent1.ScanZebra(BroadcastIntent1.Devices.Category[BroadcastIntent1.DeviceIndex],
     procedure(ABody, AScanSource, AScanData, AScanDecode: string)
    begin
       Memo1.Text := ABody;
       lblScanSource.Text := AScanSource;
       lblScanData.Text := AScanData;
       edtScan.Text := AScanData;
       lblScanDecoder.Text := AScanDecode;
    end);
    end;

     if deviceCount=1 then
     begin
            BroadcastIntent1.ScanDatalogic(BroadcastIntent1.Devices.Category[BroadcastIntent1.DeviceIndex],
     procedure(ABody, AScanSource, AScanData, AScanDecode: string)
    begin
       Memo1.Text := ABody;
       lblScanSource.Text := AScanSource;
       lblScanData.Text := AScanData;
       edtScan.Text := AScanData;
       lblScanDecoder.Text := AScanDecode;
    end);

     end;



    {$ENDIF}
end;

procedure TfrmMain.FormShow(Sender: TObject);
begin
  deviceCount:=0
end;

{$ENDIF}



end.
