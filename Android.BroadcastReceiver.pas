﻿unit Android.BroadcastReceiver;

interface

uses
  System.Classes
  , System.SysUtils
  , System.Generics.Collections
  , BroadcastIntent.Collection
  {$IFDEF ANDROID}
  , Androidapi.JNIBridge
  , Androidapi.JNI.App
  , Androidapi.JNI.Embarcadero
  , Androidapi.JNI.GraphicsContentViewText
  , Androidapi.JNI.JavaTypes
  , Androidapi.Helpers
  {$ENDIF}
  ;

type
  {$IFNDEF ANDROID}
  JIntent = class end;
  JContext = class end;
  JString = class end;
  {$ENDIF}
   { Defining class directly (TJavaLocal, JFMXBroadcastReceiverListener) is not good!
     Even if I put an instance in a global variable, it is deleted, and in Broadcast reception
     The application falls!
     So class derived from class not inheriting TInterfacedObject
     Define a JFMXBroadcastReceiverListener as an inner class
     This class will not be deleted automatically, so it works well!
   }
  TAfterScan = reference to procedure(ABody, AScanSource, AScanData, AScanDecode: String);

  TBroadcastIntent = class(TComponent)
  public type
    // Broadcast Receiver Notification Event
    // Since we are in JString, we can compare with Intent.ACTION_XXX with equals
    TBroadcastReceiverEvent = procedure(const iAction: JString) of object;
    TBroadcastOnReceive = procedure (Context: JContext; Intent: JIntent) of object;
    // Define Listener as JavaClass implementing JFMXBroadcastReceiver
    {$IFDEF ANDROID}
    TBroadcastReceiverListener =
      class(TJavaLocal, JFMXBroadcastReceiverListener)
    private var
      FBroadcastReceiver: TBroadcastIntent;
    public
      constructor Create(const iBroadcastReceiver: TBroadcastIntent);
      // Callback called from Broadcast Receiver
      procedure onReceive(context: JContext; intent: JIntent); cdecl;
      procedure onInternalReceive(context: JContext; intent: JIntent); cdecl;
    end;
    {$ENDIF}
  private var
    // The next two will disappear and disappear if you do not save!
    {$IFDEF ANDROID}
    FBroadcastReceiverListener: JFMXBroadcastReceiverListener;
    FReceiver: JFMXBroadcastReceiver;
    {$ENDIF}
    // Variable holding ACTION to be notified
    FActions: TList<String>;
    // Event handler
    FOnReceived: TBroadcastReceiverEvent;
    fOnBroadcastReceived: TBroadcastOnReceive;
    //fIntentAction: string;
    fScanDecoder: String;

    fScanData: String;
    fResponseBody: String;
    fScanSource: String;
    fOnZebraBroadcastReceived: TBroadcastOnReceive;
    fOnZebraReceived: TBroadcastReceiverEvent;
    fOnStart: TNotifyEvent;
    fDevices: TIntentDevice;
    fDeviceIndex: Integer;
    fOnDatalogicBroadcastReceived: TBroadcastOnReceive;

    fOnDatalogicReceived: TBroadcastReceiverEvent;
    fUseIntentCategory: boolean;  protected
    procedure Clear;
    // Set up and release Broadcast Receiver
    procedure SetReceiver;
    procedure UnsetReceiver;

    property OnZebraReceived: TBroadcastReceiverEvent
      read fOnZebraReceived write fOnZebraReceived;
    property OnZebraBroadcastReceived: TBroadcastOnReceive read fOnZebraBroadcastReceived write fOnZebraBroadcastReceived;

    property OnDatalogicReceived: TBroadcastReceiverEvent
      read fOnDatalogicReceived write fOnDatalogicReceived;
    property OnDatalogicBroadcastReceived: TBroadcastOnReceive read fOnDatalogicBroadcastReceived write fOnDatalogicBroadcastReceived;
  protected

    procedure ZebraReceived(const iAction: JString);
    procedure ZebraBroadcastReceived(Context: JContext; Intent: JIntent);
    procedure sendZebraBroadcast(aintentname, aextra, aaction, acategory: string);
    procedure sendDatalogicBroadcast;
    procedure onScanComplete(Sender: TObject);

    procedure ResgisterDatalogicBroadcast;
    procedure DatalogicReceived(const iAction: JString);
    procedure DatalogicBroadcastReceived(Context: JContext; Intent: JIntent);
    procedure RegisterZebraBroadcast;
  public

    constructor Create(AOwner: TComponent);override;
    destructor Destroy; override;
    // Registering and deleting ACTION that you want to notify
    procedure AddAction(const iActions: array of JString);
    procedure RemoveAction(const iAction: JString);
    procedure ClearAction;

    procedure ScanZebra(ACategory: String; AProc: TAfterScan);
    procedure Scan(AInentName, AExtraIntent, AAction, ACategory: String);
    procedure ScanDatalogic(ACategory: String; AProc: TAfterScan);
    procedure EnableDisableZebra;
    property ScanSource: String read fScanSource write fScanSource;
    property ScanData: String read fScanData write fScanData;
    property ScanDecoder: String read fScanDecoder write fScanDecoder;
    property ResponseBody: String read fResponseBody write fResponseBody;
    procedure displayScanResult(Intent: JIntent; howDataReceived: String);
    procedure displayDatalogicScanResult(Intent: JIntent; howDataReceived: String);
    procedure AddZebraSettings;
    procedure AddDataLogicSettings;
  published
    //this has to add in AndroidManifest.template.xml
    //property IntentAction: string read fIntentAction write fIntentAction;
    // Event when receiving Broadcast Receiver

    property OnReceived: TBroadcastReceiverEvent
      read FOnReceived write FOnReceived;
    property OnBroadcastReceived: TBroadcastOnReceive read fOnBroadcastReceived write fOnBroadcastReceived;
    property OnBeforeStart: TNotifyEvent read fOnStart write fOnStart;
    property Devices: TIntentDevice read fDevices write fDevices;
    property DeviceIndex: Integer read fDeviceIndex write fDeviceIndex default 0;
    property UseIntentCategory: boolean read fUseIntentCategory write fUseIntentCategory;
  end;

// Return instance of Broadcast Receiver
//function BroadcastReceiver: TBroadcastIntent;

procedure Register;

const
  myZebraIntentName = 'com.symbol.datawedge.api.ACTION'; //this has to add in AndroidManifest.template.xml
  DW_EXTRA = 'com.symbol.datawedge.api.SOFT_SCAN_TRIGGER';
  DW_SOURCE = 'com.symbol.datawedge.source';
  DW_DATA_STRING = 'com.symbol.datawedge.data_string';
  DW_LABEL_TYPE = 'com.symbol.datawedge.label_type';
  DATAWEDGE_START_SCANNING = 'START_SCANNING';
  EXTRA_ENABLE_DATAWEDGE = 'com.symbol.datawedge.api.ENABLE_DATAWEDGE';
  DW_CATEGORY = 'CATEGORY_DEFAULT';

  //for datalogic
  myDataLogicIntentName = 'com.datalogic.decodewedge.decode_action'; //this has to add in AndroidManifest.template.xml
  CATEGORY_BROADCAST_RECEIVER = 'com.datalogic.decodewedge.decode_category';
  DL_BARCODE_STRING = 'com.datalogic.decode.intentwedge.barcode_string';
  DL_BARCODE_DATA = 'com.datalogic.decode.intentwedge.barcode_data';
  DL_BARCODE_TYPE = 'com.datalogic.decode.intentwedge.barcode_type';
  DL_START_SCANNING = 'com.datalogic.decodewedge.ACTION_START_DECODE';//'com.datalogic.examples.STARTINTENT';
  DL_STOP = 'com.datalogic.decodewedge.ACTION_STOP_DECODE';//'android.intent.category.DEFAULT';
  {
  DL_EXTRA = 'com.symbol.datawedge.api.SOFT_SCAN_TRIGGER';
  DL_SOURCE = 'com.symbol.datawedge.source';
  DL_DATA_STRING = 'com.symbol.datawedge.data_string';
  DL_LABEL_TYPE = 'com.symbol.datawedge.label_type';
  DATALOGIC_START_SCANNING = 'START_SCANNING';
  EXTRA_ENABLE_DATALOGIC = 'com.symbol.datawedge.api.ENABLE_DATAWEDGE';
  }

implementation

uses
  System.UITypes
  {$IFDEF ANDROID}
  , Androidapi.NativeActivity
  {$ENDIF}
  ;

procedure Register;
begin
  RegisterComponents('Android', [TBroadcastIntent]);
end;

{ TBroadcastIntent.TBroadcastReceiverListener }
{$IFDEF ANDROID}
constructor TBroadcastIntent.TBroadcastReceiverListener.Create(
  const iBroadcastReceiver: TBroadcastIntent);
begin
  inherited Create;
  FBroadcastReceiver := iBroadcastReceiver;
end;

procedure TBroadcastIntent.TBroadcastReceiverListener.onInternalReceive(
  context: JContext; intent: JIntent);
var
  JStr: String;
  Str: String;

  procedure CallEvent;
  var
    Action: String;
    fThread: TThread;
  begin
    // Broadcast is not delivered in Delphi's main thread
    // Call with Synchronize
    Action := JStr;
    fThread := TThread.CreateAnonymousThread(
      procedure
      begin
        TThread.Synchronize(
          TThread.CurrentThread,
          procedure
          begin
            if (Assigned(FBroadcastReceiver.fOnZebraReceived)) then
              FBroadcastReceiver.fOnZebraReceived(StringToJString(Action));
            if Assigned(FBroadcastReceiver.fOnZebraBroadcastReceived) then
              FBroadcastReceiver.fOnZebraBroadcastReceived(context, intent);
          end
        );
      end
    );
    fThread.OnTerminate := FBroadcastReceiver.onScanComplete;
    fThread.Start;
  end;

begin
  // If you receive a Broadcast, this method will be called!
  JStr := JStringToString(intent.getAction);

  for Str in FBroadcastReceiver.FActions do
    if (Str = JStr) then
      CallEvent;
end;

procedure TBroadcastIntent.TBroadcastReceiverListener.onReceive(
  context: JContext;
  intent: JIntent);
var
  JStr: String;
  Str: String;

  procedure CallEvent;
  var
    Action: String;
  begin
    // Broadcast is not delivered in Delphi's main thread
    // Call with Synchronize
    Action := JStr;
    TThread.CreateAnonymousThread(
      procedure
      begin
        TThread.Synchronize(
          TThread.CurrentThread,
          procedure
          begin
            if (Assigned(FBroadcastReceiver.FOnReceived)) then
              FBroadcastReceiver.FOnReceived(StringToJString(Action));
            if Assigned(FBroadcastReceiver.fOnBroadcastReceived) then
              FBroadcastReceiver.fOnBroadcastReceived(context, intent);
          end
        );
      end
    ).Start;
  end;

begin
  // If you receive a Broadcast, this method will be called!
  JStr := JStringToString(intent.getAction);

  for Str in FBroadcastReceiver.FActions do
    if (Str = JStr) then
      CallEvent;
end;
{$ENDIF}
{ TReceiverListener }

procedure TBroadcastIntent.AddAction(const iActions: array of JString);
{$IFDEF ANDROID}
var
  Str: String;
  JStr: String;
  Action: JString;
  OK: Boolean;
  Changed: Boolean;
begin
  Changed := False;

  for Action in iActions do
  begin
    OK := True;

    JStr := JStringToString(Action);

    for Str in FActions do
      if (Str = JStr) then
      begin
        OK := False;
        Break;
      end;

    if (OK) then begin
      FActions.Add(JStr);
      Changed := True;
    end;
  end;

  if (Changed) then
    SetReceiver;
  //Log.d('Exiting AddAction');
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.AddDataLogicSettings;
begin
  with fDevices.Add do
  begin
    Device := 'datalogic';
    Details.IntentString := myDataLogicIntentName;
    Details.ExtraString := DL_BARCODE_STRING;
    Details.SourceString := '';
    Details.DataString := DL_BARCODE_STRING;//DL_BARCODE_DATA;
    Details.LabelType := DL_BARCODE_TYPE;
    Details.StartScanning := DL_START_SCANNING;
    Details.EnableString := DL_STOP;
    Details.Category := CATEGORY_BROADCAST_RECEIVER;
  end;
end;

procedure TBroadcastIntent.AddZebraSettings;
begin
  fDevices.Clear;
  with fDevices.Add do
  begin
    Device := 'zebra';
    Details.IntentString := myZebraIntentName;
    Details.ExtraString := DW_EXTRA;
    Details.SourceString := DW_SOURCE;
    Details.DataString := DW_DATA_STRING;
    Details.LabelType := DW_LABEL_TYPE;
    Details.StartScanning := DATAWEDGE_START_SCANNING;
    Details.EnableString := EXTRA_ENABLE_DATAWEDGE;
    Details.Category := DW_CATEGORY;
  end;
  AddDataLogicSettings;
end;

procedure TBroadcastIntent.Clear;
begin
  if Assigned(fOnStart) then
    fOnStart(Self);
  fResponseBody := '';
  fScanDecoder := '';
  fScanData := '';
  fScanSource := '';
end;

procedure TBroadcastIntent.ClearAction;
begin
  FActions.Clear;
  UnsetReceiver;
end;

constructor TBroadcastIntent.Create(AOwner: TComponent);
begin
  inherited;
  fDevices := TIntentDevice.Create(Self);
  AddZebraSettings;
  FActions := TList<String>.Create;

  // Set Broadcast Receiver
  SetReceiver;
end;

procedure TBroadcastIntent.DatalogicBroadcastReceived(Context: JContext;
  Intent: JIntent);
begin
  displayDatalogicScanResult(Intent, 'via Broadcast');
end;

procedure TBroadcastIntent.DatalogicReceived(const iAction: JString);
begin
{$IFDEF ANDROID}
  fResponseBody := 'Broadcast Received = ' + JStringToString(iAction);
{$ENDIF}
end;

destructor TBroadcastIntent.Destroy;
begin
  // Cancel Broadcast Receiver
  UnsetReceiver;

  FActions.DisposeOf;
  fDevices.Free;
  inherited;
end;

procedure TBroadcastIntent.displayDatalogicScanResult(Intent: JIntent;
  howDataReceived: String);
{$IFDEF ANDROID}
var
  decodedData: String;
begin
  decodedData := JStringToString
    (Intent.getStringExtra(StringToJString(fDevices[fDeviceIndex].Details.DataString)));
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.displayScanResult(Intent: JIntent;
  howDataReceived: String);
{$IFDEF ANDROID}
var
  decodedSource: String;
  decodedData: String;
  decodedLabelType: String;
begin
  if fDevices.Count = 0 then
    Exit;
  decodedSource := JStringToString
    (Intent.getStringExtra(StringToJString(fDevices[fDeviceIndex].Details.SourceString)));
  decodedData := JStringToString
    (Intent.getStringExtra(StringToJString(fDevices[fDeviceIndex].Details.DataString)));
  decodedLabelType := JStringToString
    (Intent.getStringExtra(StringToJString(fDevices[fDeviceIndex].Details.LabelType)));

  fScanSource := Format('%s %s', [decodedSource, 'via Broadcast']);
  fScanData := decodedData;
  fScanDecoder := decodedLabelType;
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.EnableDisableZebra;
{$IFDEF ANDROID}
var
  ji: JIntent;
begin
  Clear;
  ji := TJIntent.JavaClass.init;
  ji.setAction(StringToJString(myZebraIntentName));
  ji.putExtra(StringToJString(DW_EXTRA),
    StringToJString(EXTRA_ENABLE_DATAWEDGE));
  TAndroidHelper.Activity.sendBroadcast(ji);
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.onScanComplete(Sender: TObject);
begin
  ClearAction;
end;

procedure TBroadcastIntent.RegisterZebraBroadcast;
begin
{$IFDEF ANDROID}
  OnZebraReceived := ZebraReceived;
  OnZebraBroadcastReceived := ZebraBroadcastReceived;
  AddAction([StringToJString(myZebraIntentName)]);
{$ENDIF}
end;

procedure TBroadcastIntent.RemoveAction(const iAction: JString);
{$IFDEF ANDROID}
var
  i: Integer;
  JStr: String;
begin
  JStr := JStringToString(iAction);

  for i := 0 to FActions.Count - 1 do
    if (FActions[i] = JStr) then
    begin
      FActions.Delete(i);
      SetReceiver;
      Break;
    end;
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.ResgisterDatalogicBroadcast;
begin
{$IFDEF ANDROID}
  OnDatalogicReceived := DatalogicReceived;
  OnDatalogicBroadcastReceived := DatalogicBroadcastReceived;
  AddAction([StringToJString(myDataLogicIntentName)]);
{$ENDIF}
end;

procedure TBroadcastIntent.ScanZebra(ACategory: String; AProc: TAfterScan);
begin
{$IFDEF ANDROID}
  try
    Clear;
    RegisterZebraBroadcast;
    sendZebraBroadcast(myZebraIntentName, DW_EXTRA, DATAWEDGE_START_SCANNING, ACategory);
  finally
    AProc(fResponseBody, fScanSource, fScanData, fScanDecoder);
  end;
{$ENDIF}
end;

procedure TBroadcastIntent.Scan(AInentName, AExtraIntent, AAction, ACategory: String);
begin
  Clear;
  if AInentName = '' then
  begin
    raise Exception.Create('Intent name is empty.');
  end;
  {$IFDEF ANDROID}
  AddAction([StringToJString(AInentName)]);
  sendZebraBroadcast(AInentName, AExtraIntent, AAction, ACategory);
  {$ENDIF}
end;

procedure TBroadcastIntent.ScanDatalogic(ACategory: String; AProc: TAfterScan);
begin
{$IFDEF ANDROID}
  try
    Clear;
    ResgisterDatalogicBroadcast;
    sendDatalogicBroadcast;
  finally
    AProc(fResponseBody, fScanSource, fScanData, fScanDecoder);
  end;
{$ENDIF}
end;

procedure TBroadcastIntent.sendDatalogicBroadcast;
{$IFDEF ANDROID}
var
  ji: JIntent;
begin
  ji := TJIntent.JavaClass.init;
  ji.setAction(StringToJString(myDataLogicIntentName));
  ji.addCategory(StringToJString(CATEGORY_BROADCAST_RECEIVER));
  ji.putExtra(StringToJString(DL_BARCODE_DATA),
    StringToJString(DL_START_SCANNING));
  TAndroidHelper.Activity.sendBroadcast(ji);
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.sendZebraBroadcast(aintentname, aextra, aaction, acategory: string);
{$IFDEF ANDROID}
var
  ji: JIntent;
begin
  ji := TJIntent.JavaClass.init;
  ji.setAction(StringToJString(aintentname));
  ji.putExtra(StringToJString(aextra),
    StringToJString(aaction));
  //if fUseIntentCategory then
  //  ji.addCategory(StringToJString(acategory));
  TAndroidHelper.Activity.sendBroadcast(ji);
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.SetReceiver;
{$IFDEF ANDROID}
var
  Filter: JIntentFilter;
  Str: String;
begin
  if (FReceiver <> nil) then
    UnsetReceiver;

  // Create Intent Filter
  Filter := TJIntentFilter.JavaClass.init;

  for Str in FActions do
    Filter.addAction(StringToJString(Str));

  // Create a BroadcastReceiver with TBroadcastReceiverListener as the entity
  //Log.d('TBroadcastReceiverListener.Create');
  FBroadcastReceiverListener := TBroadcastReceiverListener.Create(Self);
  //Log.d('TJFMXBroadcastReceiver.init');
  FReceiver :=
    TJFMXBroadcastReceiver.JavaClass.init(FBroadcastReceiverListener);

  try
    // Register as a receiver
    //Log.d('register receiver');
    TAndroidHelper.Context.getApplicationContext.registerReceiver(
      FReceiver,
      Filter);
  except
    on E: Exception do
      //Log.d('Exception: %s', [E.Message]);
  end;
  //Log.d('Exiting SetReceiver');
{$ELSE}
begin
{$ENDIF}
end;

procedure TBroadcastIntent.UnsetReceiver;
begin
  // If the application is not terminating, cancel BroadcastReceiver
  {if
    (FReceiver <> nil) and
    (not (TAndroidHelper.Context as JActivity).isFinishing)
  then}
  {$IFDEF ANDROID}
    try
      TAndroidHelper.Context.getApplicationContext.unregisterReceiver(FReceiver);
    except
    end;

  FReceiver := nil;
  {$ENDIF}
end;

procedure TBroadcastIntent.ZebraBroadcastReceived(Context: JContext;
  Intent: JIntent);
begin
  displayScanResult(Intent, 'via Broadcast');
end;

procedure TBroadcastIntent.ZebraReceived(const iAction: JString);
begin
{$IFDEF ANDROID}
  fResponseBody := 'Broadcast Received = ' + JStringToString(iAction);
{$ENDIF}
end;

end.
