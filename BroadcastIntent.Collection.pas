                                unit BroadcastIntent.Collection;

interface

uses
  SysUtils, Classes;

type
  TIntentDevice = class;

  TIntentDeviceDetail = class(TPersistent)
  private
    fSourceString: string;
    fintent: string;
    fEnableString: string;
    fStartScanning: string;
    fExtra: string;
    fDataString: string;
    fLabelType: string;
    fCategory: string;
    fEnable: boolean;
  protected
  public
    constructor Create(AOwner: TComponent);
    procedure Assign(Source: TPersistent); override;
  published
    property IntentString: string read fintent write fintent;
    property ExtraString: string read fExtra write fExtra;
    property SourceString: string read fSourceString write fSourceString;
    property DataString: string read fDataString write fDataString;
    property LabelType: string read fLabelType write fLabelType;
    property StartScanning: string read fStartScanning write fStartScanning;
    property EnableString: string read fEnableString write fEnableString;
    property Category: string read fCategory write fCategory;
    property Enable: boolean read fEnable write fEnable default false;
  end;

  TIntentDeviceItem = class(TCollectionItem)
  private
    fDevice: string;
    fDetails: TIntentDeviceDetail;
  public
    constructor Create(Collection: TCollection); override;
    destructor Destroy; override;
    procedure Assign(Source: TPersistent); override;
    function GetDisplayName: string; override;
  published
    property Device: string read fDevice write fDevice;
    property Details: TIntentDeviceDetail read fDetails write fDetails;
  end;

  TIntentDevice = class(TCollection)
  private
    FOwner: TComponent;
    function GetItem(Index: Integer): TIntentDeviceItem;
    procedure SetItem(Index: Integer; Value: TIntentDeviceItem);
    function getintent(Index: Integer): string;
    procedure setintent(Index: Integer; const Value: string);
    function getExtraString(Index: Integer): string;
    procedure setExtraString(Index: Integer; const Value: string);
    function getSourceString(Index: Integer): string;
    procedure setSourceString(Index: Integer; const Value: string);
    function getDataString(Index: Integer): string;
    procedure setDataString(Index: Integer; const Value: string);
    function getLabelType(Index: Integer): string;
    procedure setLabelType(Index: Integer; const Value: string);
    function getStartScanning(Index: Integer): string;
    procedure setStartScanning(Index: Integer; const Value: string);
    function getEnableString(Index: Integer): string;
    procedure setEnableString(Index: Integer; const Value: string);
    function getCategory(Index: Integer): string;
    procedure setCategory(Index: Integer; const Value: string);
    function getEnable(Index: Integer): boolean;
    procedure setEnable(Index: Integer; const Value: boolean);
  protected
    function GetOwner: TPersistent; override;
  public
    constructor Create(AOwner: TComponent);
    function Add: TIntentDeviceItem;
    function Insert(Index: Integer): TIntentDeviceItem;
    property Items[Index: Integer]: TIntentDeviceItem read GetItem
      write SetItem; default;

    property IntentString[Index: Integer]: string read getintent write setintent;
    property ExtraString[Index: Integer]: string read getExtraString write setExtraString;
    property SourceString[Index: Integer]: string read getSourceString write setSourceString;
    property DataString[Index: Integer]: string read getDataString write setDataString;
    property LabelType[Index: Integer]: string read getLabelType write setLabelType;
    property StartScanning[Index: Integer]: string read getStartScanning write setStartScanning;
    property EnableString[Index: Integer]: string read getEnableString write setEnableString;
    property Category[Index: Integer]: string read getCategory write setCategory;
    property Enable[Index: Integer]: boolean read getEnable write setEnable;
  end;

implementation

uses Android.BroadcastReceiver;


{ TIntentDeviceItem }

procedure TIntentDeviceItem.Assign(Source: TPersistent);
begin
  Device := (Source as TIntentDeviceItem).Device;
  Details := (Source as TIntentDeviceItem).Details;
end;

constructor TIntentDeviceItem.Create(Collection: TCollection);
begin
  inherited;
  fDetails := TIntentDeviceDetail.Create(TComponent(Collection.Owner));
end;

destructor TIntentDeviceItem.Destroy;
begin
  inherited;
end;

function TIntentDeviceItem.GetDisplayName: string;
begin
  if fDevice <> '' then
    Result := fDevice
  else
    Result := inherited;
end;

{ TIntentDevice }

function TIntentDevice.Add: TIntentDeviceItem;
begin
  Result := TIntentDeviceItem(inherited Add);
end;

constructor TIntentDevice.Create(AOwner: TComponent);
begin
  inherited Create(TIntentDeviceItem);
  FOwner := AOwner;
end;

function TIntentDevice.getCategory(Index: Integer): string;
begin
  Result := Items[Index].Details.Category;
end;

function TIntentDevice.getDataString(Index: Integer): string;
begin
  Result := Items[Index].Details.DataString;
end;

function TIntentDevice.getEnable(Index: Integer): boolean;
begin
  Result := Items[Index].Details.Enable;
end;

function TIntentDevice.getEnableString(Index: Integer): string;
begin
  Result := Items[Index].Details.EnableString;
end;

function TIntentDevice.getExtraString(Index: Integer): string;
begin
  Result := Items[Index].Details.ExtraString;
end;

function TIntentDevice.getintent(Index: Integer): string;
begin
  Result := Items[Index].Details.IntentString;
end;

function TIntentDevice.GetItem(Index: Integer): TIntentDeviceItem;
begin
  Result := TIntentDeviceItem(inherited Items[Index]);
end;

function TIntentDevice.getLabelType(Index: Integer): string;
begin
  Result := Items[Index].Details.LabelType;
end;

function TIntentDevice.GetOwner: TPersistent;
begin
  Result := FOwner;
end;

function TIntentDevice.getSourceString(Index: Integer): string;
begin
  Result := Items[Index].Details.SourceString;
end;

function TIntentDevice.getStartScanning(Index: Integer): string;
begin
  Result := Items[Index].Details.StartScanning;
end;

function TIntentDevice.Insert(Index: Integer): TIntentDeviceItem;
begin
  Result := TIntentDeviceItem(inherited Insert(Index));
end;

procedure TIntentDevice.setCategory(Index: Integer; const Value: string);
begin
  Items[Index].Details.Category := Value;
end;

procedure TIntentDevice.setDataString(Index: Integer; const Value: string);
begin
  Items[Index].Details.DataString := Value;
end;

procedure TIntentDevice.setEnable(Index: Integer; const Value: boolean);
begin
  Items[Index].Details.Enable := Value;
  if Value then
    TBroadcastIntent(FOwner).EnableDisableZebra;
end;

procedure TIntentDevice.setEnableString(Index: Integer; const Value: string);
begin
  Items[Index].Details.EnableString := Value;
end;

procedure TIntentDevice.setExtraString(Index: Integer; const Value: string);
begin
  Items[Index].Details.ExtraString := Value;
end;

procedure TIntentDevice.setintent(Index: Integer; const Value: string);
begin
  Items[Index].Details.IntentString := Value;
end;

procedure TIntentDevice.SetItem(Index: Integer; Value: TIntentDeviceItem);
begin
  inherited SetItem(Index, Value);
end;

procedure TIntentDevice.setLabelType(Index: Integer; const Value: string);
begin
  Items[Index].Details.LabelType := Value;
end;

procedure TIntentDevice.setSourceString(Index: Integer; const Value: string);
begin
  Items[Index].Details.SourceString := Value;
end;

procedure TIntentDevice.setStartScanning(Index: Integer; const Value: string);
begin
  Items[Index].Details.StartScanning := Value;
end;

{ TIntentDeviceDetail }

procedure TIntentDeviceDetail.Assign(Source: TPersistent);
begin
  inherited;

end;

constructor TIntentDeviceDetail.Create(AOwner: TComponent);
begin

end;

end.

